/*
 * File:   main_TTP229B.c
 * Author: Asus
 *
 * Created on June 23, 2020, 6:07 PM
 */

#define CLOCK_20MHZ

#include <xc.h>
#include <stdio.h>
#include "Alteri.h"
#include "PCD8544.h"
#include "TTP299B.h"

int buttons = 0;
char String[12];

void main(void) {
    ADCON1bits.PCFG = 0b1111;   //Makes Channels digital
    LCD_NOKIA_Init();           
    LCD_NOKIA_Clear();
    LCD_NOKIA_WriteString("Hello! :D",0,0);   //Test string
    TTP299B_init();
    while (1) {
        buttons = TTP299B_getAll();
        sprintf(String,"Btns: %X",buttons);
        LCD_NOKIA_WriteString(String,0,1);   //Test string
        delay_ms(50);
    }
    return;
}
