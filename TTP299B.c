/*******************************************************************************
 *                  Library: TTP299B
 *******************************************************************************
 * FileName:        TTP299B.c
 * Processor:       PICxxxxxx
 * Complier:        XC8 v2.10
 * Author:          Brandon Ruiz Vasquez
 * Email:           Branrv64@gmail.com
 * Description:     Touch button module that allows up to 16 buttons
 *                  Implements a 2-wire communication.
 *                  Tested on board HW-136
 * Board mod:       Five connections have been created in order for the board
 *                  to function efficiently
 *                  16 Buttons Mode     TP2
 *                  Multi Keys Mode     TP3, TP4
 *                  64Hz Sampling Rate  TP5
 *                  2ms Wake Up Rate    TP6
 *      *   6   5   4
 * TP       |   |   |
 *      *   6   5   4
 * 
 *      *   *   2   3
 * TP           |   |
 *      *   *   2   3
 ******************************************************************************/

#define _XTAL_FREQ  20000000
#include <xc.h>
#include "TTP299B.h"

/*
 * Function:        void TTP299B_init()
 * Description:     Initialiaze TTP229B. Sets Clock as Output and SDO as Input
 * Parameters:      None
 * Return Values:   None
 * */
void TTP299B_init(){
    TTP299B_TRIS_SCL = 0;
    TTP299B_TRIS_SDO = 1;
    TTP299B_PORT_SCL = 0;
}

/*
 * Function:        int TTP299B_getAll()
 * Description:     Reads all 16 touchpad's pad
 * Parameters:      None
 * Return Values:   Pad combinations
 * */
int TTP299B_getAll(){
    TTP299B_PORT_SCL = 1;
    int res = 0;
    int i;
    while(TTP299B_PORT_SDO);    //DV
    while(!TTP299B_PORT_SDO);
     __delay_us(10);            //TW
    for (i = 0; i < 16; i++) {
        TTP299B_PORT_SCL = 0;
        __delay_us(1);
        res = (res << 1) | TTP299B_PORT_SDO;
        TTP299B_PORT_SCL = 1;
        __delay_us(1);
    }
    return res;
}

/*
 * Function:        char TTP299B_getButton(char Button)
 * Description:     Reads specific pad from 00-15
 * Parameters:      char Button (00-15)
 * Return Values:   char button
 * */
char TTP299B_getButton(char Button){
    Button = Button & 0b1111;
    return (TTP299B_getAll() >> Button) & 0b1;
}

